.. Atomic Simulation Recipes documentation master file, created by
   sphinx-quickstart on Thu Sep  5 12:35:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :hidden:

   src/install
   src/tutorials/tutorials
   src/howtoguides/howtoguides
   src/explanations/explanations
   src/generated/recipes
   src/generated/api
   src/developing
   src/contact


Welcome to Atomic Simulation Recipes's documentation!
=====================================================
Recipes for Atomic Scale Materials Research.

Welcome to ASR's documentation. The goal of ASR is to assist
computational atomic scale materials research and accelerate
productivity. ASR seeks to achieve this by implementing a
*light-weight* Python framework of tools and :ref:`common tasks
<recipes>` that can be tailored to users needs.

First time here: :ref:`Getting started`


.. Admonition:: Overview of documentation

   The documentation is divided into four categories:
   
   * :ref:`Tutorials`: Lessons that take the reader by the hand though a
     series of steps to complete a project.
   * :ref:`How to guides`: Concrete solutions to common tasks.
   * :ref:`Explanations`: Explanations that clarify and
     illuminate a particular topic.
   * :ref:`API Reference`: Technical documentation of the code.

   See also Daniele Procida's PyCon 2017 talk "`How documentation
   works... <https://www.youtube.com/watch?v=azf6yzuJt54>`_" for a
   detailed explanation of this division.

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
